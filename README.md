# Welcome to the th_ptb Tutorial Repository
This repository provides a tutorial for [th_ptb](https://gitlab.com/thht/th_ptb), a class-library for the [Psychophysics Toolbox](http://psychtoolbox.org/).

You can find all the relevant information on the [wiki](https://gitlab.com/thht-teaching/th_ptb_tutorial/wikis/home).